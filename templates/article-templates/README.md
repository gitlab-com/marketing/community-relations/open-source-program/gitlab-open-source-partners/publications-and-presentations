# Article templates
This folder contains templates useful for composing written materials, including blog posts, articles, case studies, and book chapters.

## General guidance
We publish written materials about open source partners' use of GitLab so other open source projects can learn about GitLab's benefits directly from their peers. Therefore, when we're planning a new article, blog post, case study, or similar piece, we should always be working to answer one or more of the following questions:

* What would a reader in an open source community be *interested in learning* or *excited to hear* that an open source partner has accomplished?
* What problem has an open source partner solved using GitLab? How can other open source projects also use GitLab to solve this problem?
* What benefits has the open source partner realized by using GitLab? How can other open source projects reap those benefits by using GitLab?
* What does the open source partner's experience teach the reader? What will someone reading the piece be able *learn* and *apply* or *implement* after reading the work?
* What quantifiable data or metrics can we offer in support of our claims?

## Article types

### The Migration Announcement
We like to publish these when a GitLab Open Source Partner successfully migrates some significant aspect of its project infrastructure to GitLab.

### The Success Story
We like to publish these when an open source community overcomes a challenge they've faced by adopting and using a GitLab feature.


