# Success story article template

## About
We can publish success stories whenever a GitLab Open Source Partner uses GitLab to accomplish something it would like to share with the wider GitLab community. Examples of these accomplishments might include adding a new feature to the project, enhancing some critical facet of the project's workflow, lowering a significant barrier to contributing to the project, or launching a new initiative. For partners, these articles serve as opportunities to circulate news of their progress and successes to a wider audience. For GitLab, these articles serve as a means of demonstrating precisely how leading open source communities are using GitLab to innovate and enhance an open source ecosystem.

## Project details

* Project name: 
* Project founding date (approximate): 
* Estimated number of contributors: 

## Questions to answer

* What did your community recently use GitLab to achieve, or what challenge did you community recently overcome?
* How did using GitLab help your community achieve this goal or overcome this challenge?
* What does achieving this goal or solving this problem allow the community to do that it couldn't do before?
* What did your community learn from this process, and how might those lessons help other open source communities facing a similar challenge?

## Examples

* [How GitLab helped Kali Linux attract a growing number of community contributions](https://about.gitlab.com/blog/2021/02/18/kali-linux-movingtogitlab/)
