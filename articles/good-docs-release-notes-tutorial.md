# Creating release notes with GitLab and The Good Docs Project

Release notes allow software users to quickly understand the changes that come with the latest version of software.
They also allow software publishers to highlight changes as important, or provide crucial information about the impact an upgrade may have.
Some tools allow developers to "generate" release notes based on sources of data (such as completed items in DevOps systems), but notes produced this way tend to simply list changes without context.
Writing release notes, however, provides teams with the opportunity to "tell the story" of the changes the new software version will bring.
Though this process certainly requires a greater investment of time than publishing a basic changelog does, your users will certainly appreciate the results: release notes that explain the key elements of the release (such as new features, improvements, and known issues) in a well-organized, human-readable way.

[The Good Docs Project's](https://thegooddocsproject.dev/welcome/) release notes template is designed to help you do exactly that.
And the combination of GitLab's work management platform and our own [Release Notes template](https://gitlab.com/tgdp/templates/-/tree/main/release-notes?ref_type=heads) makes the job of putting out good, informative release notes easier.

## The anatomy of quality release notes

Release notes that provide readers with a good picture of the version's changes require two primary inputs:

- **A list of the changes included in the release.**
At The Good Docs Project, all the management of the work of our contributors occurs in GitLab.
So it's easy to refer to our release plans to identify which additions and improvements were completed and included in the release.
- **A description of those changes including reasoning, importance, and impact.**
This is where our project's Release Notes template can assist.
Rather than staring at a blank page, wondering where to start, users can begin to fill in our template step-by-step, adjusting to taste.

We'll walk through each of these steps in the following sections as they occurred when creating the release notes to [our recent Dragon release](https://gitlab.com/tgdp/templates/-/releases/v1.1.0).

## Gathering a release's changes

At The Good Docs Project, we use GitLab features—including setting milestones, creating/assigning issues, and tagging releases—to get our work out into the community (see our [prior blog post here at GitLab that describes this process](https://about.gitlab.com/blog/2023/08/24/coordinating-documentation-projects-gitlab/)).
The platform allows our worldwide contributor base to easily discover new things to work on and update everyone on their progress once they select something.
When the time comes to package a release, it brings the added benefit of a tidy list of issues included in the project at the time of release.

![The Milestone screen in GitLab provides an easy-to-scan list of work included in the release](./gitlab-tgdp-release-notes-milestone.png)

When creating the release notes for our project's Dragon milestone, we reviewed all the items included in the **Closed** column on the Milestone screen.
This allowed us to pick the most important changes to highlight, while leaving out issues that wouldn't significantly impact a user's experience.

## Crafting the release notes

Equipped with a list of all the key updates in the release, we start writing the release notes.
Our project's [Release Notes template](https://gitlab.com/tgdp/templates/-/blob/main/release-notes/template-release-notes.md?ref_type=heads) provides a ready-made Markdown skeleton comprised of key sections based on our contributors' research and experience.
The accompanying [usage guide](https://gitlab.com/tgdp/templates/-/blob/main/release-notes/guide-release-notes.md?ref_type=heads) and [example of the template in action](https://gitlab.com/tgdp/templates/-/blob/main/release-notes/example-release-notes.md?ref_type=heads) provides additional tips and suggestions for writing effective release notes.
The latter references our **Chronologue** project, a fictional telescope and application that can see through time, which is naturally well-documented.

![The Release Notes template comes ready to populate with "the story" of your latest release](./gitlab-tgdp-release-notes-template.png)

Of course, our template is simply a starting point.
Teams should always feel free to add sections where they make sense, remove them where they don't, and make the style of it their own.
For example, we left out the **Bug fixes** and **Known issues** sections in our latest Dragon release notes, instead focusing on the new additions and improvements this release brought.

## Adding release notes to the release

GitLab's build tools also make it easy to add our notes while actually creating the release.
First, we tagged one of our project's commits, then created a release from the tag.
On GitLab's **Releases > New** screen, we can copy and paste the Markdown we wrote to automatically format the release notes.

![Our templates are already in Markdown format, so when it's time to paste them into the release it works automagically!](gitlab-tgdp-release-notes-release.png)

And just like that our release notes are done.
With the assistance of the template, they required just an hour to write.
And after an additional half-hour of work creating the release, we're ready to send our work out to the community.
Our experience using the combination of GitLab and our templates has made the process of shipping our templates a piece of cake.

If you'd like to check out our templates, feel free to browse [our GitLab project](https://gitlab.com/tgdp).
Or visit our [community page](https://thegooddocsproject.dev/community/) to learn how to join us in leveling up the state of technical documentation.

*The [GitLab Open Source Partners](https://go.gitlab.com/030Ue3) are building the future of open source on GitLab. [Connect with them](https://gitlab.com/gitlab-com/marketing/developer-relations/open-source-program/gitlab-open-source-partners) on Gitlab.com.*
