# GitLab Open Source Partner Presentations

A running list of presentations by and about GitLab Open Source Partners

## Webcasts

### 2023

* [Building Manjaro with GitLab](https://www.youtube.com/watch?v=Rn5IiI3--Ag)
* [A healthier open source ecosystem, one doc at a time: A conversation with The Good Docs Project](https://www.youtube.com/watch?v=Bek7vLmNmME)
* [How The Open Group uses GitLab to build open technology standards](https://youtu.be/0--qGhH-MBQ)
* [Using GitLab to power citizen journalism](https://youtu.be/4wIg2M1EoHI)

### 2022

* [KDE empowers open source community with GitLab](https://youtu.be/aLDFlXSI0Qs)

### 2021

* [VLC migration to GitLab ](https://youtu.be/B4fP9OA6Dw4)
