# GitLab Open Source Partner Publications
A running list of publications by and about GitLab Open Source Partners

## GitLab blog articles

### 2024

* 2024-01-23: [Pair GitLab and The Good Docs Project template to improve release notes](https://about.gitlab.com/blog/2024/01/23/pair-gitlab-and-the-good-docs-project-template-to-improve-release-notes/) | `️https://go.gitlab.com/J0CGn5`

### 2023

* 2023-10-19: [How the Eclipse Foundation champions open source with GitLab](https://about.gitlab.com/blog/2023/10/19/how-eclipse-foundation-champions-open-source-with-gitlab/) | `https://go.gitlab.com/gHBzgb`
* 2023-09-27: [How the Colmena project uses GitLab to support citizen journalists](https://about.gitlab.com/blog/2023/09/27/open-source-tools-for-citizen-journalists/) | `https://go.gitlab.com/p9xzwa`
* 2023-09-18: [Debian customizes CI tooling with GitLab](https://about.gitlab.com/blog/2023/09/19/debian-customizes-ci-tooling-with-gitlab/) | `https://go.gitlab.com/ZIntFC`
* 2023-09-11: [Migrating Arch Linux's packaging infrastructure to GitLab](https://about.gitlab.com/blog/2023/09/11/migrating-arch-linux-packaging-infrastructure-gitlab/) | `https://go.gitlab.com/uRFfYc`
* 2023-08-29: [Why the Manjaro Linux distribution builds with GitLab](https://about.gitlab.com/blog/2023/08/29/why-manjaro-builds-with-gitlab/) | `https://go.gitlab.com/S9BE8B`
* 2023-08-24: [Coordinating major documentation projects with GitLab](https://about.gitlab.com/blog/2023/08/24/coordinating-documentation-projects-gitlab/) | `https://go.gitlab.com/nDK0oV`
* 2023-07-11: [How building modern websites with GitLab led to a healthier Fedora Project community](https://about.gitlab.com/blog/2023/07/11/building-new-fedora-project-website-with-gitlab/) | `https://go.gitlab.com/gG5R6m`
* 2023-07-06: [How The Good Docs Project uses GitLab for documentation as code and more](https://about.gitlab.com/blog/2023/07/06/meet-partner-the-good-docs-project/) | `https://go.gitlab.com/16yEa3`
* 2023-06-20: [Get to know our newest open source partner, The Open Group](https://about.gitlab.com/blog/2023/06/20/interview-the-open-group/) | `https://go.gitlab.com/pe1ROx`
* 2023-01-30: [Start an open source center of excellence in 10 minutes using GitLab](https://about.gitlab.com/blog/2023/01/30/how-start-ospo-ten-minutes-using-gitlab/) | `https://go.gitlab.com/fhKvUa`

### 2021

* 2021-02-18: [How GitLab helped Kali Linux attract a growing number of community contributions](https://about.gitlab.com/blog/2021/02/18/kali-linux-movingtogitlab/)

### 2020

* 2020-09-08: [GNOME: two years after the move to GitLab](https://about.gitlab.com/blog/2020/09/08/gnome-follow-up/)
* 2020-06-29: [Why the KDE community is #movingtogitlab](https://about.gitlab.com/blog/2020/06/29/welcome-kde/)
* 2020-05-15: [Announcing 32/64-bit Arm Runner Support for AWS Graviton2](https://about.gitlab.com/blog/2020/05/15/gitlab-arm-aws-graviton2-solution/)

### 2019

* 2019-12-03: [Welcoming OpenCores to GitLab](https://about.gitlab.com/blog/2019/12/03/welcoming-opencores-to-gitlab/)
* 2019-10-08: [DevOps on the edge: Upcoming collaborations between GitLab and Arm](https://about.gitlab.com/blog/2019/10/08/devops-on-the-edge-a-conversation-about-gitlab-and-arm/)

### 2018

* 2022-08-16: [Come on in! Drupal is moving to GitLab](https://about.gitlab.com/blog/2018/08/16/drupal-moves-to-gitlab/)

## Case studies

* [GitLab accelerates innovation and improves efficiency for Synchrotron SOLEIL](https://about.gitlab.com/customers/synchrotron_soleil/) | `https://go.gitlab.com/HesQjW`
* [How SKA uses GitLab to help construct the world’s largest telescope](https://about.gitlab.com/customers/square_kilometre_array/) | `https://go.gitlab.com/5nEcqT`
* [Drupal Association eases entry for new committers, speeds implementations](https://about.gitlab.com/customers/drupalassociation/) | `https://go.gitlab.com/q0YkrU`
